import java.util.*;

public class SchoolClass {
    public static void main(String[] args) {
        String[] масивУчнів = {"Антонов", "Шевченко", "Бойко", "Шевченко", "Кравченко", "Савицький", "Антонов", "Шевченко", "Тетерів", "Мамченко"};
        List<String> списокУчнів = new ArrayList<>();
        for (String учень : масивУчнів) {
            списокУчнів.add(учень);
        }
        Set<String> прізвищаУчнів = new HashSet<>();
        for (String учень : масивУчнів)
            прізвищаУчнів.add(учень);

        Map<String, Integer> прізвище = new HashMap<>();
        for(String учень : списокУчнів) {
            прізвище.put(учень, учень.length());
        }
    }
}
